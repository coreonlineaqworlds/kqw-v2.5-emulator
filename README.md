**Database and Files**

First setup the database and the files inorder to make the emulator working.

*The emulator is capable of using any database names and server names.*
*We recommend that you need to use mextv2 database because KQW isn't a mextv3 server. Example: meh_servers NOT servers.*

---

## Features

You’ll start by creating KQW v2.5 special features.

## Adminer Database
---
Webshop
---

1. Click **Create table** 
2. Type the table name "meh_web_shop".
3. Select the engine to "MyISAM".
4. Select the collation to "utf8_general_ci"
**Tables**

5. Column names. "id, itemid, Costs, Locked, quantity"
6. Type. " id = int, itemid = int, Costs = int, Locked = int, quantity = tinyint.
7. Length. " id = 11, itemid = 50, Costs = 25, Locked = 1, quantity = 255.
8. Options, NULL, AI. Leave blank
9. Default value. " id = leave blank, itemid = leave blank, Costs = leave blank, Locked = 0 " checked ", quantity = 0 " checked ".
10. Check default values and click save.
---
Daily Login
---

Daily Login is hidden in the Alogious Game Menu and the Alogious Client. ( Eximous )

---
Username Color
---

Username color is only available in the coding of DreamOfTree Client.
Available Colors:
Blue
Lime
Pink
Yellow
Default
and more... except for "Rainbow and Red"

For v2.4
All available colors that is coded in the Emulator except for " Red ".

## Setup

Let's setup the meh_servers
1. Just add a table name or a server normally with the IP, MOTD and some things.
2. If you don't have a MonthlyQReset and DailyQReset, don't worry. We will provide you a template of the v2.5 KQW Database

Next, now it is time to setup the configuration files

**Config.xml**
1. Edit the config.xml and put the IP address * if you are running a Non Hamachi server but if you are running a Hamachi server put the Hamachi IP or if you are running by yourself put "localhost"
2. Then scroll down and you can see the you can see the ConnectionString. In the connectionstring, replace the " name?" to your database name. Example. the database name is kqw. so the connectionstring is "kqw?"
3. Replace the Username to your MySQL username but the default is (root)
4. The same as no.3 but replace the Password to your MySQL Password.
5. Then Click Save.

**.conf Files**
1. Now edit the the .conf files. but the following conf files are Asplit.conf
2. Edit the file normally.
3. Then click save again.
---

## Click start.bat